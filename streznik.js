var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	var davcna=req.param('ds');
	var ime=req.param('ime');
	var priimek=req.param('priimek');
	var naslov=req.param('ulica');
	var hisna=req.param('hisnaStevilka');
	var postna=req.param('postnaStevilka');
	var kraj=req.param('kraj');
	var drzava=req.param('drzava');
	var poklic=req.param('poklic');
	var telefonska=req.param('telefonskaStevilka');
	if(davcna=='' ||ime==''||priimek==''||naslov==''|| hisna==''||postna==''||kraj==''||drzava==''||poklic==''||telefonska==''){
		res.send("Napaka pri dodajanju osebe!");
	}else{
		var obstaja=-1;
		uporabnikiSpomin.forEach(function(item){
			if(item.davcnaStevilka==davcna)obstaja=item.davcnaStevilka;
		});
		if(obstaja==-1){
			var novUporabnik={
				davcnaStevilka: davcna,
				ime: ime,
				priimek: priimek,
				naslov:naslov,
				hisnaStevilka: hisna,
				postnaStevilka: postna,
				kraj: kraj,
				drzava: drzava,
				poklic: poklic,
				telefonskaStevilka: telefonska
			};
			uporabnikiSpomin.push(novUporabnik);
			res.redirect('/seznam.html');
		}else{
			var response="Oseba z davčno številko "+ obstaja+" že obstaja!<br/><a href='javascript:window.history.back()'>Nazaj</a>";
			res.send(response);
		}
	}

	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	var davcna=req.param('id');
	var indeks=-1;
	uporabnikiSpomin.forEach(function(item,i){
		if(item.davcnaStevilka == davcna)indeks=i;
	});
	if(davcna==='' || davcna===undefined){
		res.send('Napacna zahteva');
	}else if(indeks==-1){
		var response="Oseba z davčno številko"+davcna+" ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>";
		res.send(response);	res.send('Napacna zahteva');
	}else{
		uporabnikiSpomin.splice(indeks, 1);
		res.redirect('/seznam.html');
	}
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];